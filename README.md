### USE YARN:

```
    npm i -g yarn
```

### INSTALL DEPENDENCIES:

```
    yarn
```

### RUN:

```
    yarn dev
```
