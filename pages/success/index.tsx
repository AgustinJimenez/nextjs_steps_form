import React from "react";
import Layout from "../../components/Layout";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";

const useStyles = makeStyles((theme) => ({
  paper: {
    width: "100%",
    paddingBottom: theme.spacing(5),
    borderTopColor: theme.palette.primary.main,
    borderTopWidth: theme.spacing(0.5),
    borderTopStyle: "solid",
  },
  success_label: {
    color: green[500],
  },
}));

const SuccessPage = ({}) => {
  const classes = useStyles();
  return (
    <Layout>
      <Grid container item xs={12} spacing={2}>
        <Paper elevation={3} className={classes.paper}>
          <Grid container alignItems="center" justify="center">
            <Grid container item justify="center">
              <Typography
                variant="h6"
                gutterBottom
                className={classes.success_label}
              >
                Your info was saved
              </Typography>
            </Grid>
            <Grid container item justify="center">
              <Button variant="contained" color="primary" href="/">
                Go Back
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default SuccessPage;
