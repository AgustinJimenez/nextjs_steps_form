import React from "react";
import Layout from "../components/Layout";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";

interface FormInterface {
  full_name: string;
  age: number;
  country: string;
  email: string;
  linkedin_url: string;
  phone: string;
  experience: string;
  telephone: string;
  company_name: string;
  company_website_url: string;
}
const useStyles = makeStyles((theme) => ({
  button: {
    marginRight: theme.spacing(1),
  },
  paper: {
    paddingBottom: theme.spacing(5),
    borderTopColor: theme.palette.primary.main,
    borderTopWidth: theme.spacing(0.5),
    borderTopStyle: "solid",
  },
}));

const steps = [
  {
    label: "Personal Information",
  },
  {
    label: "Company Information",
  },
  {
    label: "Experience",
  },
];

const Home = ({}) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [form, setForm] = React.useState<FormInterface>({
    full_name: "",
    age: 18,
    country: "canada",
    email: "",
    linkedin_url: "",
    phone: "",
    company_name: "",
    company_website_url: "",
    telephone: "",
    experience: "",
  });

  const updateField = React.useCallback(
    (event) => {
      setForm((state) => {
        let field_name = event.target.name;
        let value = event.target.value;
        state[field_name] = value;
        return state;
      });
    },
    [form, setForm]
  );

  const handleBack = React.useCallback(() => {
    setActiveStep(activeStep - 1);
  }, [activeStep, setActiveStep]);
  const handleNext = React.useCallback(() => {
    if (activeStep === steps.length - 1) {
    } else setActiveStep(activeStep + 1);
  }, [activeStep, setActiveStep]);

  return (
    <Layout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper elevation={3} className={classes.paper}>
            <Grid container spacing={2} alignItems="center" justify="center">
              <Grid item xs={12}>
                <Stepper activeStep={activeStep}>
                  {steps.map(({ label }, key) => (
                    <Step key={key} completed={false}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </Grid>
              <Grid
                container
                item
                xs={8}
                spacing={2}
                justify="center"
                alignItems="center"
              >
                <span style={{ display: activeStep === 0 ? "block" : "none" }}>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <TextField
                        label="FullName"
                        type="text"
                        required
                        name="full_name"
                        placeholder="Enter your fullname"
                        onChange={updateField}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <TextField
                        label="Age"
                        type="number"
                        required
                        name="age"
                        placeholder="Enter your age"
                        onChange={updateField}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl variant="standard" fullWidth>
                      <InputLabel>Country</InputLabel>
                      <Select onChange={updateField} name="country">
                        <MenuItem value="usa">
                          United States of America
                        </MenuItem>
                        <MenuItem value="canada">Canada</MenuItem>
                        <MenuItem value="mexico">Mexico</MenuItem>
                        <MenuItem value="japan">Japan</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </span>
                <span style={{ display: activeStep === 1 ? "block" : "none" }}>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <TextField
                        label="Company name"
                        type="text"
                        required
                        name="company_name"
                        placeholder="Enter name"
                        onChange={updateField}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <TextField
                        label="Telephone"
                        type="tel"
                        required
                        name="telephone"
                        placeholder="Enter telephone"
                        onChange={updateField}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <TextField
                        label="Website Url"
                        type="url"
                        name="company_website_url"
                        placeholder="Enter url"
                        onChange={updateField}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                </span>
                <span style={{ display: activeStep === 2 ? "block" : "none" }}>
                  <Grid item xs={12}>
                    <FormControl>
                      <TextField
                        label="Describe your Experience"
                        multiline
                        rows={4}
                        name="experience"
                        placeholder="Describe your Experience"
                        onChange={updateField}
                        variant="outlined"
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                </span>
              </Grid>
            </Grid>
          </Paper>
        </Grid>

        <Grid container item xs={12} alignItems="flex-end" justify="flex-end">
          <Grid item>
            <Button
              disabled={!activeStep}
              onClick={handleBack}
              className={classes.button}
              variant="contained"
              color="primary"
            >
              Back
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={handleNext}
              className={classes.button}
            >
              {activeStep === steps.length - 1 ? "Submit" : "Next"}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};
export default Home;

export async function getStaticProps() {
  return {
    props: {},
  };
}
