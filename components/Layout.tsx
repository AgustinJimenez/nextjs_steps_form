import Head from "next/head";
import styles from "./layout.module.css";
import CssBaseline from "@material-ui/core/CssBaseline";
import clsx from "clsx";

export const siteTitle = "Next.js Sample Website";
const Layout = ({ children }) => {
  return (
    <div className={clsx([styles.container, "bg"])}>
      <CssBaseline />
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="og:title" content={siteTitle} />
      </Head>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
